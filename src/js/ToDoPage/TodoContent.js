import React from 'react';

const ToDoContent = (props) => {
    return (
    	<div>{props.content}</div>
    );
}

export default ToDoContent;
